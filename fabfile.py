
from fabric.api import *


def assembly():
    local('''sbt assembly''')


def submit(config_file):
    local("spark-submit target/scala-2.10/Ingestion-assembly-0.1.0-SNAPSHOT.jar {}".format(config_file))