import sbt._

object Dependencies {

  lazy val mainDependencies =
    Seq(
      "com.databricks" % "spark-csv_2.10" % "1.5.0",
      "com.typesafe.scala-logging" % "scala-logging-slf4j_2.10" % "2.1.2",
      "com.typesafe" % "config" % "1.3.1",
      "org.slf4j" % "slf4j-api" % "1.7.7",

      "org.apache.spark" % "spark-core_2.10" % "1.6.0" % "provided",
      "org.apache.spark" % "spark-sql_2.10" % "1.6.0" % "provided",
      "org.apache.spark" % "spark-hive_2.10" % "1.6.0" % "provided"
    )

  lazy val testDependencies =
    Seq(
      "org.scalatest" %% "scalatest" % "3.0.1" % "test"
    )

}
