# Guide
## Overview
Run ETL jobs based on typesafe config file.

The config file should contain 3 section as seen below:

```
sources: [ ]  // list sources to Extract from go here 

transforms: [ ] // list of sql transformation go here

sinks: [ ] // list of sinks to Load to go here
```

## TLDR
1. assembly the uber jar 
    `sbt assembly`
2. submit a spark job based by supplying the config file
    `spark-submit target/scala-2.10/Ingestion-assembly-0.1.0-SNAPSHOT.jar job-configs/etl-job.conf`
    
Or you could just run:
    `fab assembly submit:job-configs/etl-job.conf` after installing [fabric](http://www.fabfile.org/)

## Architecture
The current approach is to store each dataset as dataframe in spark and then handle each transformation using sql instead of using RDD api instead.
This approach is not written in stone and there is an issue open in order to address the implications of this approach. [here](https://gitlab.com/avandrade86/ingestion/issues/11)  


## Components and spec
### Source

```
sources: [                                   // Specifies a list of sources.
  {
    type: file                              // source type. Possible values: [file] 
    format: csv                             // file format. Possible values: [csv]
    path: datasets/data/ml-1m/ratings.dat   // input path 
    id: raw_ratings                         // used by spark-sql as temp table name. Will be referenced later in `Tranforms` and `Sinks` 
  }
]
```
### Transform

```
transforms: [
  {
    type: sql                               // transform type. Possible values: [sql]
    id: ratings                             // used by spark-sql as temp table name. Will be referenced later in `Tranforms` and `Sinks`
    stmt: "select * from raw_ratings"       // sql transform statement
  }
]
```

### Sink

```
sinks: [     
     {
       type: parquet                            // sink type. Possible values: [parquet, console]
       dataframe: users_top_3                   // references the `id` of the dataframe in `source` or `sink`
       path: "data/output/users_top_3.parquet"  // output path 
     }
   ]
```

## Build
`sbt assembly` - generated an uber jar in `target/scala-2.10/<build>.jar`. (default)

## Run
`spark-submit target/scala-2.10/Ingestion-assembly-0.1.0-SNAPSHOT.jar <config_file>`

## For future work references see [issues](https://gitlab.com/avandrade86/ingestion/boards)




