package com.avandrade.transform

import com.avandrade.common.SparkSetup
import org.scalatest.{FlatSpec, Matchers}

import SparkSetup._

class SqlTransformSpec extends FlatSpec with Matchers {

  behavior of "SqlTransformSpec"

  it should "return the same count as raw dataFrame" in {
    df.registerTempTable("raw_user_data")
    val countTransform = new SqlTransform("SqlTransform".toLowerCase, "select count(*) from raw_user_data")

    countTransform.dataFrame.first().getLong(0) shouldEqual df.count()
  }

}
