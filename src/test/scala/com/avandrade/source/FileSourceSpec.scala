package com.avandrade.source

import java.io.File

import com.avandrade.common.SparkSetup
import org.scalatest.{FlatSpec, Matchers}
import SparkSetup._

class FileSourceSpec extends FlatSpec with Matchers {

  behavior of "FileSourceSpec"

  it should "return the same count as raw dataFrame" in {
    df.registerTempTable("raw_user_data")

    val file = getClass.getClassLoader.getResource("testdata/users.dat").getPath

    val fileSource = new FileSource(format="csv", delimiter=None, header=None, path = file, schema="content:string", id="raw_data")

    fileSource.dataFrame.count() shouldEqual df.count()
  }

}
