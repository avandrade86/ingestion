package com.avandrade.common

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext}

object SparkSetup {

  implicit lazy val spark = new SparkContext(new SparkConf().setMaster("local[2]").setAppName("test"))

  implicit lazy val sqlContext = new SQLContext(spark)


  lazy val df: DataFrame = sqlContext.read.text(getClass.getClassLoader.getResource("testdata/users.dat").getPath)

}
