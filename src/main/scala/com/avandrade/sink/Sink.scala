package com.avandrade.sink

import com.typesafe.config.Config
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.apache.spark.sql.{DataFrame, SQLContext}

import scala.util.Try

trait Sink extends LazyLogging {
  def write(implicit sQLContext: SQLContext): Unit
}

class ConsoleSink(val dataframe: String, truncate:Option[Boolean], numRows:Option[Int]=Some(20)) extends Sink {
  override def write(implicit sQLContext: SQLContext): Unit = {
    sQLContext.sql(s"SELECT * FROM ${dataframe}").show(numRows.getOrElse(20),truncate=truncate.getOrElse(false))
  }
}

object ConsoleSink {
  def apply(config: Config) =
    new ConsoleSink(
      dataframe = config.getString("dataframe"),
      truncate = Try(config.getBoolean("truncate")).toOption,
      numRows = Try(config.getInt("numRows")).toOption
    )
}

class ParquetSink(val dataframe: String, path: String) extends Sink {
  override def write(implicit sQLContext: SQLContext): Unit = {
    sQLContext.sql(s"SELECT * FROM ${dataframe}").write.parquet(path)
  }
}

object ParquetSink {
  def apply(config: Config) =
    new ParquetSink(
      dataframe = config.getString("dataframe"),
      path = config.getString("path")
    )
}

