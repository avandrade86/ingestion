package com.avandrade.util

import java.io.File

import com.avandrade.sink.{ConsoleSink, ParquetSink, Sink}
import com.avandrade.source.{FileSource, Source}
import com.avandrade.transform.{SqlTransform, Transform}
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.slf4j.LazyLogging

import scala.collection.JavaConverters._


object ConfigReader extends LazyLogging {

  def apply(configFile: String) = {
    logger.debug(s"configFile: [$configFile]")
    ConfigFactory.parseFile(new File(configFile))
  }

  def sources(config: Config): Seq[Source] = {
    val sources =
      config.getObjectList("sources").asScala.map(_.toConfig).map(c => (c.getString("type"), c))

    sources
      .map{ case("file", c) => FileSource(c) }

  }

  def transforms(config: Config): Seq[Transform] = {
    val transforms =
      config.getObjectList("transforms").asScala.map(_.toConfig).map(c => (c.getString("type"), c))

    transforms
      .map{ case("sql", c) => SqlTransform(c) }
  }

  def sinks(config: Config): Seq[Sink] = {
    val sinks =
      config.getObjectList("sinks").asScala.map(_.toConfig).map(c => (c.getString("type"), c))

    sinks
      .map{
        case("console", c) =>
          ConsoleSink(c)
        case("parquet", c) =>
          ParquetSink(c)
      }
  }

}
