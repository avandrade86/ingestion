package com.avandrade.source

import com.typesafe.config.Config
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, SQLContext}

import scala.util.Try

trait Source {
   def dataFrame(implicit sqlContext: SQLContext): DataFrame
}

class FileSource(val format: String, val delimiter: Option[String], val header: Option[String], val path: String, schema: String, val id: String) extends Source {

  override def dataFrame(implicit sqlContext: SQLContext): DataFrame = {
    val reader = sqlContext
      .read
      .format("com.databricks.spark.csv")

      delimiter map { reader.option("delimiter", _) }
      header map { reader.option("delimiter", _) }

      val schemaFields = schema.split(",").map { s => val t = s.split(":"); (t(0), t(1)) }

      val structType = StructType( schemaFields map { case (n, t) => StructField(n, StringType) } )

      reader.schema(structType)

      reader.load(path)
  }

}

object FileSource {
  def apply(config: Config) =
    new FileSource(
      format = config.getString("format"),
      delimiter = Try(config.getString("delimiter")).toOption,
      header = Try(config.getString("header")).toOption,
      path = config.getString("path"),
      schema = Try(config.getString("schema")).toOption.getOrElse("content:string"),
      id = config.getString("id")
    )
}

