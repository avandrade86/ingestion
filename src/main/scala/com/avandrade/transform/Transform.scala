package com.avandrade.transform

import com.typesafe.config.Config
import org.apache.spark.sql.{DataFrame, SQLContext}

trait Transform {
   def dataFrame(implicit sqlContext: SQLContext): DataFrame
}

class SqlTransform(val id: String, val stmt: String) extends Transform {

  override def dataFrame(implicit sqlContext: SQLContext): DataFrame = {
    val df = sqlContext
      .sql(stmt)

    df.registerTempTable(id)
    df
  }

}

object SqlTransform {
  def apply(config: Config) =
    new SqlTransform(
      id = config.getString("id"),
      stmt = config.getString("stmt")
    )
}

