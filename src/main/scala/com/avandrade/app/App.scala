package com.avandrade.app

import com.avandrade.sink.ConsoleSink
import com.avandrade.source.FileSource
import com.avandrade.transform.SqlTransform
import com.avandrade.util.ConfigReader
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}

object App extends LazyLogging {
  def main(args: Array[String]): Unit = {

    logger.info("Application started")

    val conf = new SparkConf().setAppName("appName")
    implicit val spark = new SparkContext(conf)
    implicit val sparkSql = new HiveContext(spark)

    val configFile = args(0)
    logger.info(s"Loading config file: [$configFile]")
    val pipelineConfig = ConfigReader(configFile)

    val sources = ConfigReader.sources(pipelineConfig).collect { case src: FileSource => src }
    val transforms = ConfigReader.transforms(pipelineConfig).collect { case trf: SqlTransform => trf }
    val sinks = ConfigReader.sinks(pipelineConfig)

    val sourceToDataFrame =
      sources
        .map(src => (src, src.dataFrame))
        .toMap

    sourceToDataFrame
      .foreach { case (src, df) =>
        df.registerTempTable(src.id) }

    val transformToDataframe =
      transforms
        .map(trf => (trf, trf.dataFrame))
        .toMap

    transformToDataframe
      .foreach { case (trf, df) =>
        df.registerTempTable(trf.id) }

    sinks.foreach { _.write }

    logger.info("Application finished")

  }
}
