package com.avandrade.parsers

import org.apache.spark.sql._
import org.apache.spark.sql.types.StructType

trait Parser extends (DataFrame => DataFrame) {
  def schema: StructType
  def delimiter: String
  def dataframe: DataFrame
}

