import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.avandrade",
      scalaVersion := "2.10.4",
      version      := "0.1.0-SNAPSHOT"
    )),

    name := "Ingestion",

    libraryDependencies ++= mainDependencies ++ testDependencies
  )

parallelExecution in Test := false // see: https://stackoverflow.com/questions/41887273/sparkcontext-error-running-in-sbt-there-is-already-an-rpcendpoint-called-localb

